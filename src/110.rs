#![allow(dead_code)]

pub struct Solution;
// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}
use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn is_balanced(root: Option<Rc<RefCell<TreeNode>>>) -> bool {
        Solution::bheight(root).is_some()
    }

    fn bheight(root: Option<Rc<RefCell<TreeNode>>>) -> Option<usize> {
        root.map_or(Some(0), |root| {
            let mut node = root.borrow_mut();
            Solution::bheight(node.left.take()).map_or(None, |left| {
                Solution::bheight(node.right.take()).map_or(None, |right| {
                    let (min, max) = (left.min(right), left.max(right));
                    if max - min <= 1 {
                        Some(max + 1)
                    } else {
                        None
                    }
                })
            })
        })
    }
}

pub fn main() {
    use std::collections::HashMap;
    let nodes = HashMap::from([
        (&3, Rc::new(RefCell::new(TreeNode::new(3)))),
        (&7, Rc::new(RefCell::new(TreeNode::new(7)))),
        (&9, Rc::new(RefCell::new(TreeNode::new(9)))),
        (&15, Rc::new(RefCell::new(TreeNode::new(15)))),
        (&20, Rc::new(RefCell::new(TreeNode::new(20)))),
    ]);
    nodes[&3].borrow_mut().left = Some(Rc::clone(&nodes[&9]));
    nodes[&3].borrow_mut().right = Some(Rc::clone(&nodes[&20]));
    nodes[&20].borrow_mut().right = Some(Rc::clone(&nodes[&15]));
    nodes[&20].borrow_mut().right = Some(Rc::clone(&nodes[&7]));
    println!("{}", Solution::is_balanced(Some(Rc::clone(&nodes[&3]))));

    let nodes = HashMap::from([
        (&1, Rc::new(RefCell::new(TreeNode::new(1)))),
        (&2, Rc::new(RefCell::new(TreeNode::new(2)))),
        (&21, Rc::new(RefCell::new(TreeNode::new(21)))),
        (&3, Rc::new(RefCell::new(TreeNode::new(3)))),
        (&31, Rc::new(RefCell::new(TreeNode::new(31)))),
        (&4, Rc::new(RefCell::new(TreeNode::new(4)))),
        (&41, Rc::new(RefCell::new(TreeNode::new(41)))),
    ]);
    nodes[&1].borrow_mut().left = Some(Rc::clone(&nodes[&2]));
    nodes[&1].borrow_mut().right = Some(Rc::clone(&nodes[&21]));
    nodes[&2].borrow_mut().left = Some(Rc::clone(&nodes[&3]));
    nodes[&2].borrow_mut().right = Some(Rc::clone(&nodes[&31]));
    nodes[&3].borrow_mut().left = Some(Rc::clone(&nodes[&4]));
    nodes[&3].borrow_mut().right = Some(Rc::clone(&nodes[&41]));
    println!("{}", Solution::is_balanced(Some(Rc::clone(&nodes[&1]))));

    println!("{}", Solution::is_balanced(None));
}
