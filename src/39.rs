#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn combination_sum(candidates: Vec<i32>, target: i32) -> Vec<Vec<i32>> {
        fn backtrack(
            candidates: &Vec<i32>,
            cur_candidate: usize,
            result: &mut Vec<Vec<i32>>,
            cur_solution: &mut Vec<usize>,
            remainder: i32,
        ) {
            if remainder == 0 {
                result.push(
                    candidates
                        .iter()
                        .zip(cur_solution.iter())
                        .flat_map(|(&cand, &count)| std::iter::repeat(cand).take(count))
                        .collect(),
                );
            } else if cur_candidate < candidates.len() {
                for (count, sum) in (0..=remainder)
                    .step_by(candidates[cur_candidate] as usize)
                    .enumerate()
                {
                    cur_solution.push(count);
                    backtrack(
                        candidates,
                        cur_candidate + 1,
                        result,
                        cur_solution,
                        remainder - sum,
                    );
                    cur_solution.pop();
                }
            }
        }
        let mut result = Vec::new();
        let mut cur_solution = Vec::new();
        backtrack(&candidates, 0, &mut result, &mut cur_solution, target);
        result
    }
}

pub fn main() {
    println!("{:?}", Solution::combination_sum(vec![2, 3, 6, 7], 7));
    println!("{:?}", Solution::combination_sum(vec![2, 3, 5], 8));
    println!("{:?}", Solution::combination_sum(vec![2], 1));
}
