#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn calculate(s: String) -> i32 {
        let mut prevs = Vec::new();
        let mut negs = Vec::new();
        let mut prev = None;
        let mut operand = 0;
        let mut neg = false;
        for c in s.chars() {
            match c.to_digit(10) {
                Some(d) => operand = operand * 10 + d as i32,
                None => match c {
                    '(' => {
                        negs.push(std::mem::take(&mut neg));
                        prevs.push(std::mem::take(&mut prev));
                    }
                    ')' => {
                        operand = prev.unwrap_or(0) + if neg { -operand } else { operand };
                        prev = prevs.pop().unwrap();
                        neg = negs.pop().unwrap();
                    }
                    '+' | '-' => {
                        prev = Some(prev.unwrap_or(0) + if neg { -operand } else { operand });
                        neg = c == '-';
                        operand = 0;
                    }
                    _ => (),
                },
            }
        }
        prev.unwrap_or(0) + if neg { -operand } else { operand }
    }

    pub fn calculate_recursive(s: String) -> i32 {
        fn calc(chars: &mut std::str::Chars<'_>) -> i32 {
            let mut lhs = None;
            let mut cur = 0;
            let mut neg = false;
            while let Some(c) = chars.next() {
                match c.to_digit(10) {
                    Some(d) => cur = cur * 10 + d as i32,
                    None => match c {
                        '(' => cur = calc(chars),
                        ')' => break,
                        '+' | '-' => {
                            lhs = Some(lhs.unwrap_or(0) + if neg { -cur } else { cur });
                            neg = c == '-';
                            cur = 0;
                        }
                        _ => (),
                    },
                }
            }
            lhs.unwrap_or(0) + if neg { -cur } else { cur }
        }
        calc(&mut s.chars())
    }
}

pub fn main() {
    println!("{}", Solution::calculate("1 + 1".into()));
    println!("{}", Solution::calculate(" 2-1 + 2 ".into()));
    println!("{}", Solution::calculate("(1+(4+5+2)-3)+(6+8)".into()));
    println!("{}", Solution::calculate("(1+(-4+5+2)-3)+(6+8)".into()));
}
