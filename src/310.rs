#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn find_min_height_trees(n: i32, edges: Vec<Vec<i32>>) -> Vec<i32> {
        let n = n as usize;
        let mut neighbors = vec![Vec::new(); n];
        let mut degree = vec![0; n];
        for e in edges {
            let (a, b) = (e[0] as usize, e[1] as usize);
            neighbors[a].push(b);
            neighbors[b].push(a);
            degree[a] += 1;
            degree[b] += 1;
        }
        let mut q = degree
            .iter()
            .enumerate()
            .filter_map(|(node, degree)| match degree.cmp(&2) {
                std::cmp::Ordering::Less => Some(node),
                _ => None,
            })
            .collect::<Vec<_>>();
        let mut remaining = n;
        while remaining > 2 {
            let mut next_level = Vec::new();
            remaining -= q.len();
            for &node in &q {
                for &neighbor in &neighbors[node] {
                    degree[neighbor] -= 1;
                    if degree[neighbor] == 1 {
                        next_level.push(neighbor);
                    }
                }
            }
            q = next_level;
        }
        q.iter().map(|&e| e as i32).collect()
    }
}

pub fn main() {
    println!(
        "{:?}",
        Solution::find_min_height_trees(4, vec![vec![1, 0], vec![1, 2], vec![1, 3]])
    );
    println!(
        "{:?}",
        Solution::find_min_height_trees(
            6,
            vec![vec![3, 0], vec![3, 1], vec![3, 2], vec![3, 4], vec![5, 4]]
        )
    );
    println!(
        "{:?}",
        Solution::find_min_height_trees(
            7,
            vec![
                vec![0, 1],
                vec![1, 2],
                vec![1, 3],
                vec![2, 4],
                vec![3, 5],
                vec![4, 6],
            ]
        )
    );
}
