#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn longest_palindrome(s: String) -> i32 {
        let mut odd_l = vec![false; 26];
        let mut odd_u = vec![false; 26];
        let mut even_count = 0;
        let mut odd_count = 0;
        for c in s.chars() {
            let odd = if c.is_ascii_lowercase() {
                &mut odd_l[c as usize - 'a' as usize]
            } else {
                &mut odd_u[c as usize - 'A' as usize]
            };
            *odd = !*odd;
            if *odd {
                odd_count += 1;
            } else {
                odd_count -= 1;
                even_count += 1;
            }
        }
        even_count * 2 + if odd_count > 0 { 1 } else { 0 }
    }
}

pub fn main() {
    println!("{}", Solution::longest_palindrome("abccccdd".into()));
    println!("{}", Solution::longest_palindrome("a".into()));
}
