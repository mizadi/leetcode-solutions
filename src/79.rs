#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn exist(board: Vec<Vec<char>>, word: String) -> bool {
        fn exist_at(
            b: &Vec<Vec<char>>,
            i: usize,
            j: usize,
            w: &[char],
            m: &mut Vec<Vec<bool>>,
        ) -> bool {
            b[i][j] == w[0]
                && (w.len() == 1 || {
                    m[i][j] = true;
                    i > 0 && !m[i - 1][j] && exist_at(b, i - 1, j, &w[1..], m)
                        || i < b.len() - 1 && !m[i + 1][j] && exist_at(b, i + 1, j, &w[1..], m)
                        || j > 0 && !m[i][j - 1] && exist_at(b, i, j - 1, &w[1..], m)
                        || j < b[i].len() - 1 && !m[i][j + 1] && exist_at(b, i, j + 1, &w[1..], m)
                        || {
                            m[i][j] = false;
                            false
                        }
                })
        }
        let word = word.chars().collect::<Vec<_>>();
        (0..board.len()).any(|i| {
            (0..board[i].len()).any(|j| {
                exist_at(
                    &board,
                    i,
                    j,
                    &word,
                    &mut vec![vec![false; board[i].len()]; board.len()],
                )
            })
        })
    }
}

pub fn main() {
    let board = vec![
        vec!['A', 'B', 'C', 'E'],
        vec!['S', 'F', 'C', 'S'],
        vec!['A', 'D', 'E', 'E'],
    ];
    println!("{}", Solution::exist(board, "ABCCED".into()));

    let board = vec![
        vec!['A', 'B', 'C', 'E'],
        vec!['S', 'F', 'C', 'S'],
        vec!['A', 'D', 'E', 'E'],
    ];
    println!("{}", Solution::exist(board, "SEE".into()));

    let board = vec![
        vec!['A', 'B', 'C', 'E'],
        vec!['S', 'F', 'C', 'S'],
        vec!['A', 'D', 'E', 'E'],
    ];
    println!("{}", Solution::exist(board, "ABCB".into()));
}
