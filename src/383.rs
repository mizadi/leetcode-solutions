#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn can_construct(ransom_note: String, magazine: String) -> bool {
        let mut count = vec![0; 26];
        for c in magazine.chars() {
            count[c as usize - 'a' as usize] += 1;
        }
        for c in ransom_note.chars() {
            let e = &mut count[c as usize - 'a' as usize];
            if *e == 0 {
                return false;
            }
            *e -= 1;
        }
        true
    }
}

pub fn main() {
    println!("{}", Solution::can_construct("a".into(), "b".into()));
    println!("{}", Solution::can_construct("aa".into(), "ab".into()));
    println!("{}", Solution::can_construct("aa".into(), "aab".into()));
}
