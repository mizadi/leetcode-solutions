#![allow(dead_code)]

struct Solution;

// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn build_tree(preorder: Vec<i32>, inorder: Vec<i32>) -> Option<Rc<RefCell<TreeNode>>> {
        fn build(preorder: &[i32], inorder: &[i32]) -> Option<Rc<RefCell<TreeNode>>> {
            match preorder.get(0) {
                None => None,
                Some(root_val) => {
                    let root_pos = inorder.iter().position(|e| e == root_val).unwrap();
                    Some(Rc::new(RefCell::new(TreeNode {
                        val: *root_val,
                        left: build(&preorder[1..root_pos + 1], &inorder[..root_pos]),
                        right: build(&preorder[root_pos + 1..], &inorder[root_pos + 1..]),
                    })))
                }
            }
        }
        build(&preorder, &inorder)
    }
}

pub fn main() {
    println!(
        "{:#?}",
        Solution::build_tree(vec![3, 9, 20, 15, 7], vec![9, 3, 15, 20, 7])
    );
    println!("{:#?}", Solution::build_tree(vec![-1], vec![-1]));
}
