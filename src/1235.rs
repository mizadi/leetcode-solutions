#![allow(dead_code)]

struct Solution;

use std::cmp::Ordering;
impl Solution {
    pub fn job_scheduling(start_time: Vec<i32>, end_time: Vec<i32>, profit: Vec<i32>) -> i32 {
        let n = start_time.len();
        let mut indices = (0..n).collect::<Vec<_>>();
        indices.sort_unstable_by(|&i, &j| start_time[i].cmp(&start_time[j]));
        let mut max_profit = vec![0; n + 1];
        for i in (0..n).rev() {
            max_profit[i] = max_profit[i + 1].max(
                profit[indices[i]]
                    + max_profit[indices[i + 1..]
                        .binary_search_by(|&index| match start_time[index] < end_time[indices[i]] {
                            true => Ordering::Less,
                            false => Ordering::Greater,
                        })
                        .err()
                        .unwrap()
                        + (i + 1)],
            );
        }
        max_profit[0]
    }
}

pub fn main() {
    let start_time = vec![1, 2, 3, 3];
    let end_time = vec![3, 4, 5, 6];
    let profit = vec![50, 10, 40, 70];
    println!("{}", Solution::job_scheduling(start_time, end_time, profit));

    let start_time = vec![1, 2, 3, 4, 6];
    let end_time = vec![3, 5, 10, 6, 9];
    let profit = vec![20, 20, 100, 70, 60];
    println!("{}", Solution::job_scheduling(start_time, end_time, profit));

    let start_time = vec![1, 1, 1];
    let end_time = vec![2, 3, 4];
    let profit = vec![5, 6, 4];
    println!("{}", Solution::job_scheduling(start_time, end_time, profit));
}
