#![allow(dead_code)]

use std::usize;

pub struct Solution;
impl Solution {
    pub fn num_islands(mut grid: Vec<Vec<char>>) -> i32 {
        fn is_island(grid: &mut Vec<Vec<char>>, i: i32, j: i32) -> bool {
            if i < 0
                || j < 0
                || i as usize >= grid.len()
                || j as usize >= grid[i as usize].len()
                || grid[i as usize][j as usize] == '0'
            {
                return false;
            }
            grid[i as usize][j as usize] = '0';
            is_island(grid, i - 1, j);
            is_island(grid, i + 1, j);
            is_island(grid, i, j - 1);
            is_island(grid, i, j + 1);
            true
        }

        (0..grid.len()).fold(0, |count, i| {
            count
                + (0..grid[i].len()).fold(0, |count, j| {
                    count + is_island(&mut grid, i as i32, j as i32) as i32
                })
        })
    }
}

pub fn main() {
    let grid = vec![
        vec!['1', '1', '1', '1', '0'],
        vec!['1', '1', '0', '1', '0'],
        vec!['1', '1', '0', '0', '0'],
        vec!['0', '0', '0', '0', '0'],
    ];
    println!("{}", Solution::num_islands(grid));

    let grid = vec![
        vec!['1', '1', '0', '0', '0'],
        vec!['1', '1', '0', '0', '0'],
        vec!['0', '0', '1', '0', '0'],
        vec!['0', '0', '0', '1', '1'],
    ];
    println!("{}", Solution::num_islands(grid));
}
