#![allow(dead_code)]

pub struct Solution;
#[derive(Debug)]
struct MyQueue {
    s1: Vec<i32>,
    s2: Vec<i32>,
}

/**
 * `&self` means the method takes an immutable reference.
 * If you need a mutable reference, change it to `&mut self` instead.
 */
impl MyQueue {
    fn new() -> Self {
        Self {
            s1: Vec::new(),
            s2: Vec::new(),
        }
    }

    fn push(&mut self, x: i32) {
        while let Some(v) = self.s2.pop() {
            self.s1.push(v);
        }
        self.s1.push(x);
    }

    fn pop(&mut self) -> i32 {
        while let Some(v) = self.s1.pop() {
            self.s2.push(v);
        }
        self.s2.pop().unwrap()
    }

    fn peek(&mut self) -> i32 {
        while let Some(v) = self.s1.pop() {
            self.s2.push(v);
        }
        *self.s2.last().unwrap()
    }

    fn empty(&self) -> bool {
        self.s1.is_empty() && self.s2.is_empty()
    }
}

/**
 * Your MyQueue object will be instantiated and called as such:
 * let obj = MyQueue::new();
 * obj.push(x);
 * let ret_2: i32 = obj.pop();
 * let ret_3: i32 = obj.peek();
 * let ret_4: bool = obj.empty();
 */

pub fn main() {
    let mut q = MyQueue::new();
    println!("{:?}", q);
    q.push(1);
    println!("{:?}", q);
    q.push(2);
    println!("{:?}", q);
    println!("peek: {}", q.peek());
    println!("{:?}", q);
    println!("pop: {}", q.pop());
    println!("{:?}", q);
    println!("empty: {}", q.empty());
    println!("{:?}", q);
}
