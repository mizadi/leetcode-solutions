#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn permute(nums: Vec<i32>) -> Vec<Vec<i32>> {
        let n = nums.len();
        let mut result = Vec::with_capacity((1..n).fold(1, |f, n| f * (n + 1)));
        let mut perm: Vec<usize> = (0..n).collect();
        loop {
            result.push(perm.iter().map(|&e| nums[e]).collect());
            let mut inc = 1;
            while inc < n && perm[inc - 1] > perm[inc] {
                inc += 1;
            }
            if inc >= n {
                break result;
            }
            let pos = perm[0..inc]
                .binary_search_by(|e| perm[inc].cmp(e))
                .unwrap_err();
            perm.swap(inc, pos);
            perm[0..inc].reverse();
        }
    }
}

pub fn main() {
    println!("{:?}", Solution::permute(vec![1, 2, 3]));
    println!("{:?}", Solution::permute(vec![0, 1]));
    println!("{:?}", Solution::permute(vec![1]));
}
