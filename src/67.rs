#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn add_binary(a: String, b: String) -> String {
        let mut c = Vec::with_capacity(a.len().max(b.len()) + 1);
        let mut a = a.chars().rev();
        let mut b = b.chars().rev();
        let mut carry = false;
        loop {
            let (a, b) = match (a.next(), b.next()) {
                (Some(a), Some(b)) => (a == '1', b == '1'),
                (Some(a), None) => (a == '1', false),
                (None, Some(b)) => (false, b == '1'),
                (None, None) => break,
            };
            c.push(a ^ b ^ carry);
            carry = a && b || a && carry || b && carry;
        }
        if carry {
            c.push(true);
        }
        c.into_iter().rev().map(|c| if c { '1' } else { '0' }).collect()
    }
}

pub fn main() {
    println!("{}", Solution::add_binary("11".into(), "1".into()));
    println!("{}", Solution::add_binary("1010".into(), "1011".into()));
    println!("{}", Solution::add_binary("0".into(), "0".into()));
}

