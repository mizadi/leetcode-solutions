#![allow(dead_code)]

pub struct Solution;
// The API isBadVersion is defined for you.
// isBadVersion(version:i32)-> bool;
// to call it use self.isBadVersion(version)

impl Solution {
    pub fn first_bad_version(&self, n: i32) -> i32 {
        let (mut start, mut end) = (0, n);
        while start < end {
            println!("{} - {}", start, end);
            let mid = start + (end - start) / 2;
            if self.isBadVersion(mid + 1) {
                end = mid;
            } else {
                start = mid + 1;
            }
        }
        start + 1
    }

    #[allow(non_snake_case)]
    fn isBadVersion(&self, version: i32) -> bool {
        version >= 1702766719
    }
}

pub fn main() {
    let s = Solution;
    println!("{}", s.first_bad_version(2126753390));
}
