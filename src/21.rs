#![allow(dead_code)]

#[derive(PartialEq, Eq, Clone, Debug)]
struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

pub struct Solution;
impl Solution {
    fn merge_two_lists(
        list1: Option<Box<ListNode>>,
        list2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        let mut l1 = list1;
        let mut l2 = list2;
        let mut result: Option<Box<ListNode>> = None;
        let mut result_tail = &mut result;
        loop {
            match (l1, l2) {
                (Some(mut n1), Some(mut n2)) => {
                    if n1.val < n2.val {
                        l1 = n1.next.take();
                        l2 = Some(n2);
                        *result_tail = Some(n1);
                    } else {
                        l1 = Some(n1);
                        l2 = n2.next.take();
                        *result_tail = Some(n2);
                    }
                    result_tail = &mut result_tail.as_mut().unwrap().next;
                }
                (Some(n1), None) => {
                    *result_tail = Some(n1);
                    break;
                }
                (None, Some(n2)) => {
                    *result_tail = Some(n2);
                    break;
                }
                (None, None) => break,
            }
        }
        result
    }
}

fn print(list: &Option<Box<ListNode>>) {
    let mut l = list;
    println!("[");
    while let Some(ref n) = l {
        println!("{}", n.val);
        l = &n.next;
    }
    println!("]");
    println!();
}

pub fn main() {
    let list1 = None;
    print(&list1);

    let l23 = Box::new(ListNode::new(0));
    let list2 = Some(l23);
    print(&list2);

    let result = Solution::merge_two_lists(list1, list2);
    print(&result);
}
