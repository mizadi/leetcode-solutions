#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn unique_paths(m: i32, n: i32) -> i32 {
        std::iter::repeat(())
            .take(m.max(n) as usize - 1)
            .fold(vec![1; m.min(n) as usize - 1], |mut count, _| {
                count.iter_mut().fold(1, |prev, e| {
                    *e += prev;
                    *e
                });
                count
            })
            .pop()
            .unwrap_or(1)
    }
}

pub fn main() {
    println!("{}", Solution::unique_paths(3, 7));
    println!("{}", Solution::unique_paths(3, 2));
}
