#![allow(dead_code)]

pub struct Solution;

struct MinStack {
    inner: Vec<(i32, i32)>,
}

impl MinStack {
    fn new() -> Self {
        Self { inner: Vec::new() }
    }

    fn push(&mut self, val: i32) {
        self.inner.push((
            val,
            self.inner
                .last()
                .map(|&(_, min)| min.min(val))
                .unwrap_or(val),
        ));
    }

    fn pop(&mut self) {
        self.inner.pop();
    }

    fn top(&self) -> i32 {
        self.inner.last().unwrap().0
    }

    fn get_min(&self) -> i32 {
        self.inner.last().unwrap().1
    }
}

pub fn main() {
    let mut s = MinStack::new();
    println!(
        "{:?}",
        s.inner.iter().map(|(val, _)| val).collect::<Vec<_>>()
    );
    s.push(-2);
    println!(
        "{:?}",
        s.inner.iter().map(|(val, _)| val).collect::<Vec<_>>()
    );
    s.push(0);
    println!(
        "{:?}",
        s.inner.iter().map(|(val, _)| val).collect::<Vec<_>>()
    );
    s.push(-3);
    println!(
        "{:?}",
        s.inner.iter().map(|(val, _)| val).collect::<Vec<_>>()
    );
    let m = s.get_min();
    println!("{m}");
    s.pop();
    println!(
        "{:?}",
        s.inner.iter().map(|(val, _)| val).collect::<Vec<_>>()
    );
    let t = s.top();
    println!("{t}");
    let m = s.get_min();
    println!("{m}");
}
