#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn trap(height: Vec<i32>) -> i32 {
        let (mut left, mut right) = (0, 0);
        let mut iter = height.iter();
        let mut sum = 0;
        loop {
            if left < right {
                match iter.next_back() {
                    Some(&h) if h > left => left = h,
                    Some(&h) => sum += left - h,
                    None => break,
                }
            } else {
                match iter.next() {
                    Some(&h) if h > right => right = h,
                    Some(&h) => sum += right - h,
                    None => break,
                }
            }
        }
        sum
    }
}

pub fn main() {
    println!(
        "{}",
        Solution::trap(vec![0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1])
    );
    println!("{}", Solution::trap(vec![4, 2, 0, 3, 2, 5]));
}
