#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn is_palindrome(s: String) -> bool {
        let mut chars = s.chars().filter_map(|v| {
            if v.is_ascii_alphanumeric() {
                Some(v.to_ascii_lowercase())
            } else {
                None
            }
        });
        while let (Some(a), Some(b)) = (chars.next(), chars.next_back()) {
            if a != b {
                return false;
            }
        }
        true
    }
}

pub fn main() {
    println!(
        "{}",
        if Solution::is_palindrome("0P".into()) {
            "yes"
        } else {
            "no"
        }
    );
}
