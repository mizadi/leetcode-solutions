#![allow(dead_code)]

pub struct Solution;

struct Trie {
    end: bool,
    children: [Option<Box<Trie>>; 26],
}

impl Trie {
    fn new() -> Self {
        Self {
            end: false,
            children: Default::default(),
        }
    }

    fn insert(&mut self, word: String) {
        let mut node = self;
        for ch in word.chars() {
            let index = ch as usize - 'a' as usize;
            node = node.children[index].get_or_insert_with(|| Box::new(Trie::new()));
        }
        node.end = true;
    }

    fn search(&self, word: String) -> bool {
        let mut node = self;
        for ch in word.chars() {
            let index = ch as usize - 'a' as usize;
            if let Some(child) = &node.children[index] {
                node = child;
            } else {
                return false;
            }
        }
        node.end
    }

    fn starts_with(&self, prefix: String) -> bool {
        let mut node = self;
        for ch in prefix.chars() {
            let index = ch as usize - 'a' as usize;
            if let Some(child) = &node.children[index] {
                node = child;
            } else {
                return false;
            }
        }
        true
    }
}

pub fn main() {
    let mut trie = Trie::new();
    println!("{:?}", trie.insert("apple".into()));
    println!("{:?}", trie.search("apple".into()));
    println!("{:?}", trie.search("app".into()));
    println!("{:?}", trie.starts_with("app".into()));
    println!("{:?}", trie.insert("app".into()));
    println!("{:?}", trie.search("app".into()));
}
