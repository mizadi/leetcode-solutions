#![allow(dead_code)]

pub struct Solution;
// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}
use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn lowest_common_ancestor(
        root: Option<Rc<RefCell<TreeNode>>>,
        p: Option<Rc<RefCell<TreeNode>>>,
        q: Option<Rc<RefCell<TreeNode>>>,
    ) -> Option<Rc<RefCell<TreeNode>>> {
        enum Result {
            Found(Rc<RefCell<TreeNode>>),
            NotFound(bool, bool),
        }
        fn traverse(
            root: Rc<RefCell<TreeNode>>,
            p: Rc<RefCell<TreeNode>>,
            q: Rc<RefCell<TreeNode>>,
        ) -> Result {
            let (mut p_found, mut q_found) = (root == p, root == q);
            if let Some(ref left) = root.borrow().left {
                match traverse(Rc::clone(&left), Rc::clone(&p), Rc::clone(&q)) {
                    Result::Found(lca) => {
                        return Result::Found(lca);
                    }
                    Result::NotFound(pf, qf) => {
                        p_found |= pf;
                        q_found |= qf;
                    }
                }
            }
            if let Some(ref right) = root.borrow().right {
                match traverse(Rc::clone(&right), p, q) {
                    Result::Found(lca) => {
                        return Result::Found(lca);
                    }
                    Result::NotFound(pf, qf) => {
                        p_found |= pf;
                        q_found |= qf;
                    }
                }
            }
            if p_found && q_found {
                Result::Found(root)
            } else {
                Result::NotFound(p_found, q_found)
            }
        }
        if let Result::Found(lca) = traverse(root.unwrap(), p.unwrap(), q.unwrap()) {
            Some(lca)
        } else {
            None
        }
    }
}

pub fn main() {
    use std::collections::HashMap;
    let nodes = HashMap::from([
        (0, Rc::new(RefCell::new(TreeNode::new(0)))),
        (1, Rc::new(RefCell::new(TreeNode::new(1)))),
        (2, Rc::new(RefCell::new(TreeNode::new(2)))),
        (3, Rc::new(RefCell::new(TreeNode::new(3)))),
        (4, Rc::new(RefCell::new(TreeNode::new(4)))),
        (5, Rc::new(RefCell::new(TreeNode::new(5)))),
        (6, Rc::new(RefCell::new(TreeNode::new(6)))),
        (7, Rc::new(RefCell::new(TreeNode::new(7)))),
        (8, Rc::new(RefCell::new(TreeNode::new(8)))),
    ]);
    nodes[&3].borrow_mut().left = Some(Rc::clone(&nodes[&5]));
    nodes[&3].borrow_mut().right = Some(Rc::clone(&nodes[&1]));
    nodes[&5].borrow_mut().left = Some(Rc::clone(&nodes[&6]));
    nodes[&5].borrow_mut().right = Some(Rc::clone(&nodes[&2]));
    nodes[&2].borrow_mut().left = Some(Rc::clone(&nodes[&7]));
    nodes[&2].borrow_mut().right = Some(Rc::clone(&nodes[&4]));
    nodes[&1].borrow_mut().left = Some(Rc::clone(&nodes[&0]));
    nodes[&1].borrow_mut().right = Some(Rc::clone(&nodes[&8]));
    println!(
        "{}",
        Solution::lowest_common_ancestor(
            Some(Rc::clone(&nodes[&3])),
            Some(Rc::clone(&nodes[&5])),
            Some(Rc::clone(&nodes[&1]))
        )
        .unwrap()
        .borrow()
        .val,
    );
    println!(
        "{}",
        Solution::lowest_common_ancestor(
            Some(Rc::clone(&nodes[&3])),
            Some(Rc::clone(&nodes[&5])),
            Some(Rc::clone(&nodes[&4]))
        )
        .unwrap()
        .borrow()
        .val,
    );
}
