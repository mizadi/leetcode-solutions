#![allow(dead_code)]

struct Solution;
// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn right_side_view(root: Option<Rc<RefCell<TreeNode>>>) -> Vec<i32> {
        fn dfs(node: std::cell::Ref<'_, TreeNode>, depth: usize, mut result: Vec<i32>) -> Vec<i32> {
            if depth > result.len() {
                result.push(node.val);
            }
            if let Some(right) = &node.right {
                result = dfs(right.borrow(), depth + 1, result);
            }
            if let Some(left) = &node.left {
                result = dfs(left.borrow(), depth + 1, result);
            }
            result
        }
        match root {
            Some(node) => dfs(node.borrow(), 1, Vec::new()),
            None => Vec::new(),
        }
    }
}

pub fn main() {
    let root = Some(Rc::new(RefCell::new(TreeNode {
        val: 1,
        left: Some(Rc::new(RefCell::new(TreeNode {
            val: 2,
            left: None,
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 5,
                left: None,
                right: None,
            }))),
        }))),
        right: Some(Rc::new(RefCell::new(TreeNode {
            val: 3,
            left: None,
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 4,
                left: None,
                right: None,
            }))),
        }))),
    })));
    println!("{:?}", Solution::right_side_view(root));

    let root = Some(Rc::new(RefCell::new(TreeNode {
        val: 1,
        left: None,
        right: Some(Rc::new(RefCell::new(TreeNode {
            val: 3,
            left: None,
            right: None,
        }))),
    })));
    println!("{:?}", Solution::right_side_view(root));

    let root = None;
    println!("{:?}", Solution::right_side_view(root));
}
