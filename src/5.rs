#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn longest_palindrome(s: String) -> String {
        let s = s.as_bytes();
        let n = s.len();
        let (mut max_start, mut max_end) = (0, 1);
        for i in 0..2 * n - 1 {
            let (mut start, mut end) = (i / 2 + 1, (i + 1) / 2 + 1);
            while start > 0 && end < n && s[start - 1] == s[end] {
                start -= 1;
                end += 1;
            }
            if end - start > max_end - max_start {
                max_end = end;
                max_start = start;
            }
        }

        std::str::from_utf8(&s[max_start..max_end])
            .unwrap()
            .to_string()
    }
}

pub fn main() {
    println!("{}", Solution::longest_palindrome("babad".into()));
    println!("{}", Solution::longest_palindrome("cbbd".into()));
    println!("{}", Solution::longest_palindrome("a".into()));
}
