#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn k_closest(points: Vec<Vec<i32>>, k: i32) -> Vec<Vec<i32>> {
        points
            .into_iter()
            .map(|p| (p[0] * p[0] + p[1] * p[1], p))
            .fold(std::collections::BinaryHeap::new(), |mut q, p| {
                q.push(p);
                if q.len() > k as usize {
                    q.pop();
                }
                q
            })
            .into_iter()
            .map(|(_, p)| p)
            .collect()
    }
}

pub fn main() {
    println!(
        "{:?}",
        Solution::k_closest(vec![vec![1, 3], vec![-2, 2]], 1)
    );
    println!(
        "{:?}",
        Solution::k_closest(vec![vec![3, 3], vec![5, -1], vec![-2, 4]], 2)
    );
}
