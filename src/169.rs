#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn majority_element(nums: Vec<i32>) -> i32 {
        let mut max_count = 0;
        let mut max = 0;
        for n in nums {
            if max_count == 0 {
                max = n;
            }
            if max == n {
                max_count += 1;
            } else {
                max_count -= 1;
            }
        }
        max
    }
}

pub fn main() {
    println!("{}", Solution::majority_element(vec![3, 2, 3]));
    println!("{}", Solution::majority_element(vec![2, 2, 1, 1, 1, 2, 2]));
}
