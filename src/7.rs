#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn length_of_longest_substring(s: String) -> i32 {
        let mut max = 0usize;
        let mut start = 0;
        let mut seen = std::collections::HashMap::new();
        for (end, end_ch) in s.chars().enumerate() {
            if let Some(last) = seen.insert(end_ch, end) {
                start = start.max(last + 1);
            }
            max = max.max(end - start + 1);
        }
        max as i32
    }
}

pub fn main() {
    println!("{}", Solution::length_of_longest_substring("abcabcbb".into()));
    println!("{}", Solution::length_of_longest_substring("bbbbb".into()));
    println!("{}", Solution::length_of_longest_substring("pwwkew".into()));
    println!("{}", Solution::length_of_longest_substring("a".into()));
    println!("{}", Solution::length_of_longest_substring("".into()));
}
