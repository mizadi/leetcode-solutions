#![allow(dead_code)]

struct Solution;

const LETTERS: [&str; 8] = ["abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"];
impl Solution {
    pub fn letter_combinations(digits: String) -> Vec<String> {
        fn build(digits: &[u8], cur: &mut String, mut result: Vec<String>) -> Vec<String> {
            if digits.is_empty() {
                if !cur.is_empty() {
                    result.push(cur.clone());
                }
            } else {
                for ch in LETTERS[(digits[0] - b'2') as usize].chars() {
                    cur.push(ch);
                    result = build(&digits[1..], cur, result);
                    cur.pop();
                }
            }
            result
        }
        build(digits.as_bytes(), &mut String::new(), Vec::new())
    }
}

pub fn main() {
    println!("{:?}", Solution::letter_combinations("23".into()));
    println!("{:?}", Solution::letter_combinations("".into()));
    println!("{:?}", Solution::letter_combinations("2".into()));
}
