#![allow(dead_code)]

use std::collections::HashMap;

struct TimeMap {
    data: HashMap<String, Vec<(i32, String)>>,
}

impl TimeMap {
    fn new() -> Self {
        Self {
            data: HashMap::new(),
        }
    }

    fn set(&mut self, key: String, value: String, timestamp: i32) {
        self.data
            .entry(key)
            .or_insert(Vec::new())
            .push((timestamp, value));
    }

    fn get(&self, key: String, timestamp: i32) -> String {
        match self.data.get(&key) {
            Some(key_list) => {
                let pos = match key_list.binary_search_by_key(&timestamp, |(ts, _)| *ts) {
                    Ok(pos) => pos,
                    Err(pos) => {
                        if pos == 0 {
                            return String::new();
                        }
                        pos - 1
                    }
                };
                key_list[pos].1.clone()
            }
            None => String::new(),
        }
    }
}

pub fn main() {
    let mut time_map = TimeMap::new();
    time_map.set("foo".into(), "bar".into(), 1);
    println!("{}", time_map.get("foo".into(), 1));
    println!("{}", time_map.get("foo".into(), 3));
    time_map.set("foo".into(), "bar2".into(), 4);
    println!("{}", time_map.get("foo".into(), 4));
    println!("{}", time_map.get("foo".into(), 5));
}
