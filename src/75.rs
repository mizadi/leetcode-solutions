#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn sort_colors(nums: &mut Vec<i32>) {
        let (mut start, mut one_index, mut end) = (0, 0, nums.len() - 1);
        while start <= end {
            match nums[start] {
                0 => {
                    nums.swap(start, one_index);
                    start += 1;
                    one_index += 1;
                }
                1 => start += 1,
                _ => {
                    nums.swap(start, end);
                    match end {
                        0 => break,
                        _ => end -= 1,
                    }
                }
            }
        }
    }
}

pub fn main() {
    let mut nums = vec![2, 0, 2, 1, 1, 0];
    Solution::sort_colors(&mut nums);
    println!("{nums:?}");

    let mut nums = vec![2, 0, 1];
    Solution::sort_colors(&mut nums);
    println!("{nums:?}");
}
