#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn is_valid(s: String) -> bool {
        let mut opens = Vec::new();
        for ch in s.chars() {
            match ch {
                '(' | '{' | '[' => opens.push(ch),
                ')' | '}' | ']' => {
                    if let Some(op) = opens.pop() {
                        if op != Solution::opening(ch) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                },
                _ => (),
            }
        }
        opens.is_empty()
    }

    fn opening(closing: char) -> char {
        return match closing {
            ')' => '(',
            '}' => '{',
            ']' => '[',
            _ => panic!(),
        }
    }
}

pub fn main() {
    println!("{}", Solution::is_valid("(()))".into()));
}
