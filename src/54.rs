#![allow(dead_code)]

struct Solution;

const R_DELTA: [i32; 4] = [0, 1, 0, -1];
const C_DELTA: [i32; 4] = [1, 0, -1, 0];
const R_REMAIN_DEC: [bool; 4] = [true, false, true, false];
const C_REMAIN_DEC: [bool; 4] = [false, true, false, true];

impl Solution {
    pub fn spiral_order(matrix: Vec<Vec<i32>>) -> Vec<i32> {
        let (r_count, c_count) = (matrix.len(), matrix[0].len());
        let mut result = Vec::with_capacity(r_count * c_count);
        let (mut r, mut c) = (0, -1);
        let mut state = 0;
        let (mut r_remain, mut c_remain) = (r_count, c_count);
        let mut cur_remain = c_remain;
        while cur_remain > 0 {
            for _ in 0..cur_remain {
                r += R_DELTA[state];
                c += C_DELTA[state];
                result.push(matrix[r as usize][c as usize]);
            }
            if R_REMAIN_DEC[state] {
                r_remain -= 1;
                cur_remain = r_remain;
            }
            if C_REMAIN_DEC[state] {
                c_remain -= 1;
                cur_remain = c_remain;
            }
            state = (state + 1) % 4;
        }
        result
    }
}

pub fn main() {
    println!(
        "{:?}",
        Solution::spiral_order(vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]])
    );
    println!(
        "{:?}",
        Solution::spiral_order(vec![
            vec![1, 2, 3, 4],
            vec![5, 6, 7, 8],
            vec![9, 10, 11, 12]
        ])
    );
}
