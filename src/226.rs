#![allow(dead_code)]

use std::{cell::RefCell, collections::VecDeque, rc::Rc};

type Tree = Option<Rc<RefCell<TreeNode>>>;
#[derive(Debug, PartialEq, Eq)]
struct TreeNode {
    pub val: i32,
    pub left: Tree,
    pub right: Tree,
}

impl TreeNode {
    #[inline]
    fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

pub struct Solution;
impl Solution {
    fn invert_tree(root: Tree) -> Tree {
        if let Some(node) = root {
            let left_inv = Solution::invert_tree(node.borrow_mut().left.take());
            let right_inv = Solution::invert_tree(node.borrow_mut().right.take());
            node.borrow_mut().left = right_inv;
            node.borrow_mut().right = left_inv;
            Some(node)
        } else {
            None
        }
    }
}

fn print(root: &Tree) {
    let mut result = vec![];
    if let Some(root) = root {
        let mut q = VecDeque::from([Rc::clone(root)]);
        while let Some(c) = q.pop_front() {
            let node = c.borrow();
            result.push(node.val);
            if let Some(ref left) = node.left {
                q.push_back(Rc::clone(left));
            }
            if let Some(ref right) = node.right {
                q.push_back(Rc::clone(right));
            }
        }
    }
    println!("{:?}", &result);
}

pub fn main() {
    let mut a = Some(Rc::new(RefCell::new(TreeNode {
        val: 4,
        left: Some(Rc::new(RefCell::new(TreeNode {
            val: 2,
            left: Some(Rc::new(RefCell::new(TreeNode {
                val: 1,
                left: None,
                right: None,
            }))),
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 3,
                left: None,
                right: None,
            }))),
        }))),
        right: Some(Rc::new(RefCell::new(TreeNode {
            val: 7,
            left: Some(Rc::new(RefCell::new(TreeNode {
                val: 6,
                left: None,
                right: None,
            }))),
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 9,
                left: None,
                right: None,
            }))),
        }))),
    })));

    print(&a);
    a = Solution::invert_tree(a);
    print(&a);

    let mut b = Some(Rc::new(RefCell::new(TreeNode {
        val: 2,
        left: Some(Rc::new(RefCell::new(TreeNode {
            val: 1,
            left: None,
            right: None,
        }))),
        right: Some(Rc::new(RefCell::new(TreeNode {
            val: 3,
            left: None,
            right: None,
        }))),
    })));

    print(&b);
    b = Solution::invert_tree(b);
    print(&b);

    let mut c = None;
    print(&c);
    c = Solution::invert_tree(c);
    print(&c);
}
