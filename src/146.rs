#![allow(dead_code)]

use std::{cell::RefCell, collections::HashMap, rc::Rc};

type Node = Rc<RefCell<NodeContent>>;

struct NodeContent {
    key: i32,
    val: i32,
    prev: Option<Node>,
    next: Option<Node>,
}

struct List {
    head: Option<Node>,
    tail: Option<Node>,
}

impl List {
    fn new() -> Self {
        Self {
            head: None,
            tail: None,
        }
    }

    fn push_back(&mut self, key: i32, val: i32) -> Node {
        let node = Rc::new(RefCell::new(NodeContent {
            key,
            val,
            prev: None,
            next: None,
        }));
        match self.tail.as_ref() {
            Some(tail_node) => {
                tail_node.borrow_mut().next = Some(Rc::clone(&node));
                node.borrow_mut().prev = Some(Rc::clone(tail_node));
            }
            None => {
                self.tail = Some(Rc::clone(&node));
                self.head = Some(Rc::clone(&node));
            }
        }
        self.tail = Some(Rc::clone(&node));
        node
    }

    fn remove(&mut self, node: Node) {
        match (node.borrow().prev.as_ref(), node.borrow().next.as_ref()) {
            (None, None) => {
                self.head = None;
                self.tail = None;
            }
            (None, Some(next_node)) => {
                self.head = Some(Rc::clone(next_node));
                next_node.borrow_mut().prev = None;
            }
            (Some(prev_node), None) => {
                self.tail = Some(Rc::clone(prev_node));
                prev_node.borrow_mut().next = None;
            }
            (Some(prev_node), Some(next_node)) => {
                prev_node.borrow_mut().next = Some(Rc::clone(next_node));
                next_node.borrow_mut().prev = Some(Rc::clone(prev_node));
            }
        }
    }
}

struct LRUCache {
    capacity: usize,
    list: List,
    key_to_node: HashMap<i32, Node>,
}

impl LRUCache {
    fn new(capacity: i32) -> Self {
        Self {
            capacity: capacity as usize,
            list: List::new(),
            key_to_node: HashMap::new(),
        }
    }

    fn get(&mut self, key: i32) -> i32 {
        match self.key_to_node.get_mut(&key) {
            Some(node) => {
                let val = node.borrow().val;
                self.list.remove(Rc::clone(node));
                *node = self.list.push_back(key, val);
                val
            }
            None => -1,
        }
    }

    fn put(&mut self, key: i32, value: i32) {
        let mut list = std::mem::replace(&mut self.list, List::new());
        self.key_to_node
            .entry(key)
            .and_modify(|e| {
                list.remove(Rc::clone(e));
                *e = list.push_back(key, value);
            })
            .or_insert_with(|| list.push_back(key, value));
        std::mem::swap(&mut list, &mut self.list);
        if self.key_to_node.len() > self.capacity {
            let head = Rc::clone(self.list.head.as_ref().unwrap());
            self.key_to_node.remove(&head.borrow().key);
            self.list.remove(head);
        }
    }
}

pub fn main() {
    let mut lru_cache = LRUCache::new(2);
    lru_cache.put(1, 1); // cache is {1=1}
    lru_cache.put(2, 2); // cache is {1=1, 2=2}
    println!("get: {}", lru_cache.get(1)); // return 1
    lru_cache.put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
    println!("get: {}", lru_cache.get(2)); // returns -1 (not found)
    lru_cache.put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
    println!("get: {}", lru_cache.get(1)); // return -1 (not found)
    println!("get: {}", lru_cache.get(3)); // return 3
    println!("get: {}", lru_cache.get(4)); // return 4
}
