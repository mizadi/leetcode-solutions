#![allow(dead_code)]

struct Solution;

struct Trie {
    end: bool,
    children: [Option<Box<Trie>>; 26],
}

impl Trie {
    fn new() -> Self {
        Self {
            end: false,
            children: Default::default(),
        }
    }

    fn insert(&mut self, word: &str) {
        let mut node = self;
        for ch in word.chars() {
            let index = ch as usize - 'a' as usize;
            node = node.children[index].get_or_insert_with(|| Box::new(Trie::new()));
        }
        node.end = true;
    }

    fn prefix_iter<'a>(&'a self, s: &'a [char]) -> PrefixIter {
        PrefixIter {
            node: self,
            s,
            i: 0,
        }
    }
}

struct PrefixIter<'a> {
    node: &'a Trie,
    s: &'a [char],
    i: usize,
}

impl Iterator for PrefixIter<'_> {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(&ch) = self.s.get(0) {
            self.i += 1;
            self.s = &self.s[1..];
            let index = ch as usize - 'a' as usize;
            return match self.node.children[index].as_ref() {
                Some(child) => {
                    self.node = child;
                    if !self.node.end {
                        continue;
                    }
                    Some(self.i)
                }
                None => None,
            };
        }
        None
    }
}

impl Solution {
    pub fn word_break(s: String, word_dict: Vec<String>) -> bool {
        let mut trie = Trie::new();
        for word in word_dict.iter() {
            trie.insert(word);
        }
        let s = s.chars().collect::<Vec<_>>();
        let mut breakable = vec![false; s.len() + 1];
        breakable[s.len()] = true;
        for i in (0..s.len()).rev() {
            breakable[i] |= trie.prefix_iter(&s[i..]).any(|index| breakable[i + index]);
        }
        breakable[0]
    }
}

pub fn main() {
    println!(
        "{}",
        Solution::word_break("leetcode".into(), vec!["leet".into(), "code".into()])
    );

    println!(
        "{}",
        Solution::word_break("applepenapple".into(), vec!["apple".into(), "pen".into()])
    );

    println!(
        "{}",
        Solution::word_break(
            "catsandog".into(),
            vec![
                "cats".into(),
                "dog".into(),
                "sand".into(),
                "and".into(),
                "cat".into()
            ]
        )
    );
}
