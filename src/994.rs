#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn oranges_rotting(mut grid: Vec<Vec<i32>>) -> i32 {
        let (mut q, mut fresh_count) =
            (0..grid.len()).fold((Vec::new(), 0), |(q, fresh_count), i| {
                (0..grid[i].len()).fold((q, fresh_count), |(mut q, mut fresh_count), j| {
                    match grid[i][j] {
                        1 => fresh_count += 1,
                        2 => {
                            q.push((i, j));
                        }
                        _ => (),
                    }
                    (q, fresh_count)
                })
            });
        let mut depth = if q.is_empty() { 0 } else { -1 };
        while !q.is_empty() {
            depth += 1;
            let mut next_q = Vec::new();
            while let Some((i, j)) = q.pop() {
                let m = grid.len();
                let n = grid[i].len();
                let mut try_push = |i: usize, j: usize| {
                    if grid[i][j] == 1 {
                        grid[i][j] = 2;
                        next_q.push((i, j));
                        fresh_count -= 1;
                    }
                };
                if i > 0 {
                    try_push(i - 1, j);
                }
                if i < m - 1 {
                    try_push(i + 1, j);
                }
                if j > 0 {
                    try_push(i, j - 1);
                }
                if j < n - 1 {
                    try_push(i, j + 1);
                }
            }
            std::mem::swap(&mut q, &mut next_q);
        }
        if fresh_count > 0 {
            -1
        } else {
            depth
        }
    }
}

pub fn main() {
    let grid = vec![vec![2, 1, 1], vec![1, 1, 0], vec![0, 1, 1]];
    println!("{}", Solution::oranges_rotting(grid));

    let grid = vec![vec![2, 1, 1], vec![0, 1, 1], vec![1, 0, 1]];
    println!("{}", Solution::oranges_rotting(grid));

    let grid = vec![vec![0, 2]];
    println!("{}", Solution::oranges_rotting(grid));
}
