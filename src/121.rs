#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn max_profit(prices: Vec<i32>) -> i32 {
        let mut max = 0;
        if let Some(mut start) = prices.get(0) {
            for end in prices.iter().skip(1) {
                if end < start {
                    start = end;
                } else {
                    max = max.max(end - start);
                }
            }
        }
        max
    }
}

pub fn main() {
    println!(
        "[7,1,5,3,6,4]: {}",
        Solution::max_profit(vec![7, 1, 5, 3, 6, 4])
    );
    println!("[7,6,4,3,1]: {}", Solution::max_profit(vec![7, 6, 4, 3, 1]));
}
