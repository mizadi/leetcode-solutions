#![allow(dead_code)]

use std::{cmp::Reverse, collections::BinaryHeap};

struct MedianFinder {
    small: BinaryHeap<i32>,
    large: BinaryHeap<Reverse<i32>>,
}

impl MedianFinder {
    fn new() -> Self {
        Self {
            small: BinaryHeap::new(),
            large: BinaryHeap::new(),
        }
    }

    fn add_num(&mut self, num: i32) {
        if self.small.is_empty() {
            self.small.push(num);
        } else if self.small.len() == self.large.len() {
            if num <= *self.small.peek().unwrap() {
                self.small.push(num);
            } else {
                self.large.push(Reverse(num));
                self.small.push(self.large.pop().unwrap().0);
            }
        } else {
            if num < *self.small.peek().unwrap() {
                self.small.push(num);
                self.large.push(Reverse(self.small.pop().unwrap()));
            } else {
                self.large.push(Reverse(num));
            }
        }
    }

    fn find_median(&self) -> f64 {
        let v1 = *self.small.peek().unwrap();
        let v2 = if self.small.len() == self.large.len() {
            self.large.peek().unwrap().0
        } else {
            v1
        };
        (v1 + v2) as f64 / 2_f64
    }
}

pub fn main() {
    let mut median_finder = MedianFinder::new();
    median_finder.add_num(1);
    median_finder.add_num(2);
    println!("{}", median_finder.find_median());
    median_finder.add_num(3);
    println!("{}", median_finder.find_median());
}
