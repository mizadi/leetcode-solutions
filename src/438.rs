#![allow(dead_code)]

struct Solution;

use std::{collections::HashMap, iter::Enumerate, str::Chars};

struct WindowIter<'a> {
    width: usize,
    string: &'a str,
    enter: Enumerate<Chars<'a>>,
    exit: Option<Enumerate<Chars<'a>>>,
}

impl WindowIter<'_> {
    fn new<'a>(string: &'a str, width: usize) -> WindowIter<'a> {
        WindowIter {
            width,
            string,
            enter: string.chars().enumerate(),
            exit: None,
        }
    }
}

impl Iterator for WindowIter<'_> {
    type Item = (char, Option<(usize, char)>);

    fn next(&mut self) -> Option<Self::Item> {
        let (index, enter) = match self.enter.next() {
            Some(enter) => enter,
            None => return None,
        };
        let exit = match self.exit.as_mut() {
            Some(exit) => exit.next(),
            None => {
                if index == self.width - 1 {
                    self.exit = Some(self.string.chars().enumerate());
                }
                None
            }
        };
        Some((enter, exit))
    }
}

impl Solution {
    pub fn find_anagrams(s: String, p: String) -> Vec<i32> {
        let (mut counts, p_len) = p
            .chars()
            .fold((HashMap::new(), 0), |(mut counts, p_len), ch| {
                counts.entry(ch).and_modify(|e| *e += 1).or_insert(1);
                (counts, p_len + 1)
            });
        println!("{counts:?} - {p_len}");
        let mut non_zeros = counts.len();
        let mut result = Vec::new();
        for (enter, exit) in WindowIter::new(&s, p_len) {
            println!("{enter} - {exit:?}");
            counts.entry(enter).and_modify(|e| {
                *e -= 1;
                match *e {
                    0 => non_zeros -= 1,
                    -1 => non_zeros += 1,
                    _ => (),
                }
            });
            exit.map(|(_, exit)| {
                counts.entry(exit).and_modify(|e| {
                    *e += 1;
                    match *e {
                        0 => non_zeros -= 1,
                        1 => non_zeros += 1,
                        _ => (),
                    }
                })
            });
            if non_zeros == 0 {
                result.push(exit.map(|(index, _)| 1 + index as i32).unwrap_or(0));
            }
        }
        result
    }
}

pub fn main() {
    println!(
        "{:?}",
        Solution::find_anagrams("cbaebabacd".into(), "abc".into())
    );
    println!("{:?}", Solution::find_anagrams("abab".into(), "ab".into()));
}
