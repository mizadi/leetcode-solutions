#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn max_area(height: Vec<i32>) -> i32 {
        let mut height = &height[..];
        let mut max = 0;
        while height.len() > 1 {
            let last = height.len() - 1;
            max = max.max(height[0].min(height[last]) * last as i32);
            height = if height[0] < height[last] {
                &height[1..]
            } else {
                &height[..last]
            }
        }
        max
    }
}

pub fn main() {
    println!("{}", Solution::max_area(vec![1, 8, 6, 2, 5, 4, 8, 3, 7]));
    println!("{}", Solution::max_area(vec![1, 1]));
    println!("{}", Solution::max_area(vec![0, 1]));
}
