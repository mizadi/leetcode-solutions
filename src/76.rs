#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn min_window(s: String, t: String) -> String {
        let s = s.as_bytes();
        let t = t.as_bytes();
        let mut count = t
            .iter()
            .fold(std::collections::HashMap::new(), |mut count, ch| {
                *count.entry(ch).or_insert(0) += 1;
                count
            });
        let mut remaining = t.len();
        let mut q = std::collections::VecDeque::new();
        let mut min_substr: Option<&[u8]> = None;
        for (i, ch) in s.iter().enumerate() {
            count.entry(ch).and_modify(|e| {
                q.push_back((i, ch));
                *e -= 1;
                if *e >= 0 {
                    remaining -= 1;
                }
            });
            while remaining == 0 {
                let front = q.pop_front().unwrap();
                let substr = &s[front.0..i + 1];
                min_substr = min_substr
                    .map(|e| if substr.len() < e.len() { substr } else { e })
                    .or(Some(substr));
                count.entry(front.1).and_modify(|e| {
                    *e += 1;
                    if *e > 0 {
                        remaining += 1;
                    }
                });
            }
        }
        min_substr
            .map(|e| unsafe { std::str::from_utf8_unchecked(e) }.into())
            .unwrap_or(String::new())
    }
}

pub fn main() {
    println!(
        "{}",
        Solution::min_window("ADOBECODEBANC".into(), "ABC".into())
    );
    println!("{}", Solution::min_window("a".into(), "a".into()));
    println!("{}", Solution::min_window("a".into(), "aa".into()));
    println!("{}", Solution::min_window("ABBCA".into(), "ABC".into()));
}
