#![allow(dead_code)]

struct Solution;

// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

impl Ord for Box<ListNode> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.val.cmp(&self.val)
    }
}

impl PartialOrd for Box<ListNode> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Solution {
    pub fn merge_k_lists(lists: Vec<Option<Box<ListNode>>>) -> Option<Box<ListNode>> {
        let lists = lists
            .into_iter()
            .filter_map(|list| list)
            .collect::<Vec<_>>();
        let mut heap = std::collections::BinaryHeap::from(lists);
        let mut head = heap.pop()?;
        let mut tail = &mut head;
        while !heap.is_empty() {
            if let Some(rest) = tail.next.take() {
                heap.push(rest);
            }
            let list = heap.pop().unwrap();
            tail.next = Some(list);
            tail = tail.next.as_mut().unwrap();
        }
        Some(head)
    }
}

fn print_list(mut list: Option<Box<ListNode>>) {
    print!("[ ");
    while let Some(node) = list {
        print!("{} ", node.val);
        list = node.next;
    }
    println!("]");
}

pub fn main() {
    let lists = vec![
        Some(Box::new(ListNode {
            val: 1,
            next: Some(Box::new(ListNode {
                val: 4,
                next: Some(Box::new(ListNode { val: 5, next: None })),
            })),
        })),
        Some(Box::new(ListNode {
            val: 1,
            next: Some(Box::new(ListNode {
                val: 3,
                next: Some(Box::new(ListNode { val: 4, next: None })),
            })),
        })),
        Some(Box::new(ListNode {
            val: 2,
            next: Some(Box::new(ListNode { val: 4, next: None })),
        })),
    ];
    let result = Solution::merge_k_lists(lists);
    print_list(result);

    let lists = vec![];
    let result = Solution::merge_k_lists(lists);
    print_list(result);

    let lists = vec![None];
    let result = Solution::merge_k_lists(lists);
    print_list(result);
}
