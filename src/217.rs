#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn contains_duplicate(nums: Vec<i32>) -> bool {
        let mut count = std::collections::HashSet::new();
        for i in nums.iter() {
            if !count.insert(i) {
                return true;
            }
        }
        false
    }
}

pub fn main() {
    println!("{}", Solution::contains_duplicate(vec![1,2,3,1]));
    println!("{}", Solution::contains_duplicate(vec![1,2,3,4]));
    println!("{}", Solution::contains_duplicate(vec![1,1,1,3,3,4,3,2,4,2]));
}
