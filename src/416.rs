#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn can_partition(nums: Vec<i32>) -> bool {
        let (mut total, nums) = nums.iter().fold(
            (0, Vec::with_capacity(nums.len())),
            |(total, mut nums), &num| {
                let num = num as usize;
                nums.push(num);
                (total + num, nums)
            },
        );
        match total % 2 {
            0 => {
                total = total / 2;
                let can_build = nums.iter().fold(vec![false; total], |can_build, &num| {
                    (0..total)
                        .map(|t| can_build[t] || t + 1 == num || t >= num && can_build[t - num])
                        .collect()
                });
                can_build[total - 1]
            }
            _ => false,
        }
    }
}

pub fn main() {
    println!("{}", Solution::can_partition(vec![1, 5, 11, 5]));
    println!("{}", Solution::can_partition(vec![1, 2, 3, 5]));
}
