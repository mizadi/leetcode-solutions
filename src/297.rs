#![allow(dead_code)]

// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}
use core::fmt;
use std::cell::{Ref, RefCell};
use std::rc::Rc;
struct Codec {}

impl Codec {
    fn new() -> Self {
        Self {}
    }

    fn serialize_int(&self, root: Option<Ref<TreeNode>>, result: &mut String) {
        if let Some(node) = root {
            result.push('(');
            fmt::write(result, format_args!("{}", node.val)).unwrap();
            self.serialize_int(node.left.as_ref().map(|left| left.borrow()), result);
            self.serialize_int(node.right.as_ref().map(|right| right.borrow()), result);
            result.push(')');
        } else {
            result.push('*');
        }
    }

    fn serialize(&self, root: Option<Rc<RefCell<TreeNode>>>) -> String {
        let mut result = String::new();
        self.serialize_int(root.as_ref().map(|node| node.borrow()), &mut result);
        result
    }

    fn deserialize_int(&self, data: &mut &[u8]) -> Option<Rc<RefCell<TreeNode>>> {
        if data[0] == b'*' {
            *data = &data[1..];
            return None;
        }
        *data = &data[1..];
        let mut val = 0;
        let mut neg = false;
        if data[0] == b'-' {
            neg = true;
            *data = &data[1..];
        }
        while data[0] != b'(' && data[0] != b'*' {
            let d = data[0] - b'0';
            val = val * 10 + d as i32;
            *data = &data[1..]
        }
        let mut node = TreeNode::new(if neg { -val } else { val });
        node.left = self.deserialize_int(data);
        node.right = self.deserialize_int(data);
        *data = &data[1..];
        Some(Rc::new(RefCell::new(node)))
    }

    fn deserialize(&self, data: String) -> Option<Rc<RefCell<TreeNode>>> {
        let mut bytes = data.as_bytes();
        self.deserialize_int(&mut bytes)
    }
}

pub fn main() {
    let codec = Codec::new();
    let root = Some(Rc::new(RefCell::new(TreeNode {
        val: -1,
        left: Some(Rc::new(RefCell::new(TreeNode {
            val: 2,
            left: None,
            right: None,
        }))),
        right: Some(Rc::new(RefCell::new(TreeNode {
            val: 3,
            left: Some(Rc::new(RefCell::new(TreeNode {
                val: 4,
                left: None,
                right: None,
            }))),
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 5,
                left: None,
                right: None,
            }))),
        }))),
    })));
    let data = codec.serialize(root);
    println!("{data}");
    let tree = codec.deserialize(data);
    println!("{tree:#?}");

    let root = None;
    let data = codec.serialize(root);
    println!("{data}");
    let tree = codec.deserialize(data);
    println!("{tree:#?}");
}
