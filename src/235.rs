#![allow(dead_code)]

pub struct Solution;
// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}
use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn lowest_common_ancestor(
        root: Option<Rc<RefCell<TreeNode>>>,
        p: Option<Rc<RefCell<TreeNode>>>,
        q: Option<Rc<RefCell<TreeNode>>>,
    ) -> Option<Rc<RefCell<TreeNode>>> {
        let (p, q) = (p.unwrap().borrow().val, q.unwrap().borrow().val);
        let (min, max) = (p.min(q), p.max(q));
        fn recurse(node: Rc<RefCell<TreeNode>>, min: i32, max: i32) -> Rc<RefCell<TreeNode>> {
            let val = node.borrow().val;
            if max < val {
                recurse(node.borrow_mut().left.take().unwrap(), min, max)
            } else if min > val {
                recurse(node.borrow_mut().right.take().unwrap(), min, max)
            } else {
                node
            }
        }
        Some(recurse(root.unwrap(), min, max))
    }
}

pub fn main() {
    use std::collections::HashMap;
    let nodes = HashMap::from([
        (0, Rc::new(RefCell::new(TreeNode::new(0)))),
        (2, Rc::new(RefCell::new(TreeNode::new(2)))),
        (3, Rc::new(RefCell::new(TreeNode::new(3)))),
        (4, Rc::new(RefCell::new(TreeNode::new(4)))),
        (5, Rc::new(RefCell::new(TreeNode::new(5)))),
        (6, Rc::new(RefCell::new(TreeNode::new(6)))),
        (7, Rc::new(RefCell::new(TreeNode::new(7)))),
        (8, Rc::new(RefCell::new(TreeNode::new(8)))),
        (9, Rc::new(RefCell::new(TreeNode::new(9)))),
    ]);
    nodes[&6].borrow_mut().left = Some(Rc::clone(&nodes[&2]));
    nodes[&6].borrow_mut().right = Some(Rc::clone(&nodes[&8]));
    nodes[&2].borrow_mut().left = Some(Rc::clone(&nodes[&0]));
    nodes[&2].borrow_mut().right = Some(Rc::clone(&nodes[&4]));
    nodes[&8].borrow_mut().left = Some(Rc::clone(&nodes[&7]));
    nodes[&8].borrow_mut().right = Some(Rc::clone(&nodes[&9]));
    nodes[&4].borrow_mut().left = Some(Rc::clone(&nodes[&3]));
    nodes[&4].borrow_mut().right = Some(Rc::clone(&nodes[&5]));
    println!(
        "{}",
        Solution::lowest_common_ancestor(
            Some(Rc::clone(&nodes[&6])),
            Some(Rc::clone(&nodes[&5])),
            Some(Rc::clone(&nodes[&7]))
        )
        .unwrap()
        .borrow()
        .val,
    );
}
