#![allow(dead_code)]

pub struct Solution;

// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}
use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn is_valid_bst(root: Option<Rc<RefCell<TreeNode>>>) -> bool {
        fn helper(node: Option<Rc<RefCell<TreeNode>>>, min: Option<i32>, max: Option<i32>) -> bool {
            // min: None means -infinity.
            // max: None means +infinity.
            match node {
                None => true,
                Some(node) => {
                    let val = node.borrow().val;
                    min.map(|min| val > min).unwrap_or(true)
                        && max.map(|max| val < max).unwrap_or(true)
                        && helper(node.borrow_mut().left.take(), min, Some(val))
                        && helper(node.borrow_mut().right.take(), Some(val), max)
                }
            }
        }
        helper(root, None, None)
    }
}

pub fn main() {
    let root = Some(Rc::new(RefCell::new(TreeNode {
        val: 2,
        left: Some(Rc::new(RefCell::new(TreeNode {
            val: 1,
            left: None,
            right: None,
        }))),
        right: Some(Rc::new(RefCell::new(TreeNode {
            val: 3,
            left: None,
            right: None,
        }))),
    })));
    println!("{}", Solution::is_valid_bst(root));

    let root = Some(Rc::new(RefCell::new(TreeNode {
        val: 5,
        left: Some(Rc::new(RefCell::new(TreeNode {
            val: 1,
            left: None,
            right: None,
        }))),
        right: Some(Rc::new(RefCell::new(TreeNode {
            val: 4,
            left: Some(Rc::new(RefCell::new(TreeNode {
                val: 3,
                left: None,
                right: None,
            }))),
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 6,
                left: None,
                right: None,
            }))),
        }))),
    })));
    println!("{}", Solution::is_valid_bst(root));

    let root = Some(Rc::new(RefCell::new(TreeNode {
        val: 2147483647,
        left: None,
        right: None,
    })));
    println!("{}", Solution::is_valid_bst(root));
}
