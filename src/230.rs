#![allow(dead_code)]

struct Solution;

// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

use std::{
    cell::{Ref, RefCell},
    ops::ControlFlow,
    rc::Rc,
};

impl Solution {
    pub fn kth_smallest(root: Option<Rc<RefCell<TreeNode>>>, k: i32) -> i32 {
        fn kth(node: Ref<TreeNode>, mut k: usize) -> ControlFlow<i32, usize> {
            if let Some(left) = node.left.as_ref() {
                k = kth(left.borrow(), k)?;
            }
            if k == 1 {
                return ControlFlow::Break(node.val);
            }
            k -= 1;
            if let Some(right) = node.right.as_ref() {
                k = kth(right.borrow(), k)?
            }
            ControlFlow::Continue(k)
        }
        match kth(root.unwrap().borrow(), k as usize) {
            ControlFlow::Continue(_) => panic!(),
            ControlFlow::Break(v) => v,
        }
    }
}

pub fn main() {
    let root = Some(Rc::new(RefCell::new(TreeNode {
        val: 3,
        left: Some(Rc::new(RefCell::new(TreeNode {
            val: 1,
            left: None,
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 2,
                left: None,
                right: None,
            }))),
        }))),
        right: Some(Rc::new(RefCell::new(TreeNode {
            val: 4,
            left: None,
            right: None,
        }))),
    })));
    println!("{}", Solution::kth_smallest(root, 1));

    let root = Some(Rc::new(RefCell::new(TreeNode {
        val: 5,
        left: Some(Rc::new(RefCell::new(TreeNode {
            val: 3,
            left: Some(Rc::new(RefCell::new(TreeNode {
                val: 2,
                left: Some(Rc::new(RefCell::new(TreeNode {
                    val: 1,
                    left: None,
                    right: None,
                }))),
                right: None,
            }))),
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 4,
                left: None,
                right: None,
            }))),
        }))),
        right: Some(Rc::new(RefCell::new(TreeNode {
            val: 6,
            left: None,
            right: None,
        }))),
    })));
    println!("{}", Solution::kth_smallest(root, 3));
}
