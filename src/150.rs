#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn eval_rpn(tokens: Vec<String>) -> i32 {
        let mut stack: Vec<i32> = Vec::new();
        for token in tokens.iter() {
            let num = match token.as_str() {
                "+" => stack.pop().unwrap() + stack.pop().unwrap(),
                "-" => -stack.pop().unwrap() + stack.pop().unwrap(),
                "*" => stack.pop().unwrap() * stack.pop().unwrap(),
                "/" => {
                    let (a, b) = (stack.pop().unwrap(), stack.pop().unwrap());
                    b / a
                }
                num => num.parse().unwrap(),
            };
            stack.push(num);
        }
        stack.pop().unwrap()
    }
}
pub fn main() {
    let rpn = vec!["2", "1", "+", "3", "*"];
    println!(
        "{}",
        Solution::eval_rpn(rpn.into_iter().map(String::from).collect())
    );

    let rpn = vec!["4", "13", "5", "/", "+"];
    println!(
        "{}",
        Solution::eval_rpn(rpn.into_iter().map(String::from).collect())
    );

    let rpn = vec![
        "10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+",
    ];
    println!(
        "{}",
        Solution::eval_rpn(rpn.into_iter().map(String::from).collect())
    );
}
