#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn coin_change(coins: Vec<i32>, amount: i32) -> i32 {
        let amount = amount as usize;
        let mut cur = vec![None; amount + 1];
        cur[0] = Some(0);
        for cur_amount in 0..amount {
            if let Some(cur_count) = cur[cur_amount] {
                let next_count = cur_count + 1;
                for coin in coins.iter().map(|&v| v as usize) {
                    let next_amount = cur_amount + coin;
                    if next_amount <= amount {
                        cur[next_amount] =
                            Some(cur[next_amount].map_or(next_count, |v| v.min(next_count)));
                    }
                }
            }
        }
        cur[amount].map_or(-1, |v| v as i32)
    }
}

pub fn main() {
    let coins = vec![1, 2, 5];
    let amount = 11;
    println!(
        "{amount} with {coins:?}: {}",
        Solution::coin_change(coins.clone(), amount)
    );

    let coins = vec![2];
    let amount = 3;
    println!(
        "{amount} with {coins:?}: {}",
        Solution::coin_change(coins.clone(), amount)
    );

    let coins = vec![1];
    let amount = 0;
    println!(
        "{amount} with {coins:?}: {}",
        Solution::coin_change(coins.clone(), amount)
    );
}
