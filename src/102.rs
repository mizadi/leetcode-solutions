#![allow(dead_code)]

pub struct Solution;
// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn level_order(root: Option<Rc<RefCell<TreeNode>>>) -> Vec<Vec<i32>> {
        let mut result = Vec::new();
        let mut q = std::collections::VecDeque::new();
        if let Some(root) = root {
            q.push_back(root);
        }
        while !q.is_empty() {
            let n = q.len();
            let mut level = Vec::with_capacity(n);
            for _ in 0..n {
                if let Some(node) = q.pop_front() {
                    level.push(node.borrow().val);
                    if let Some(left) = node.borrow_mut().left.take() {
                        q.push_back(left);
                    }
                    if let Some(right) = node.borrow_mut().right.take() {
                        q.push_back(right);
                    }
                }
            }
            result.push(level);
        }
        result
    }
}

pub fn main() {
    let root = Some(Rc::new(RefCell::new(TreeNode {
        val: 3,
        left: Some(Rc::new(RefCell::new(TreeNode {
            val: 9,
            left: None,
            right: None,
        }))),
        right: Some(Rc::new(RefCell::new(TreeNode {
            val: 20,
            left: Some(Rc::new(RefCell::new(TreeNode {
                val: 15,
                left: None,
                right: None,
            }))),
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 7,
                left: None,
                right: None,
            }))),
        }))),
    })));
    println!("{:?}", Solution::level_order(root));

    let root = Some(Rc::new(RefCell::new(TreeNode {
        val: 1,
        left: None,
        right: None,
    })));
    println!("{:?}", Solution::level_order(root));

    let root = None;
    println!("{:?}", Solution::level_order(root));
}
