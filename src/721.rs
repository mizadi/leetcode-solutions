#![allow(dead_code)]

struct Solution;

use std::{cell::RefCell, collections::BTreeMap, rc::Rc};

struct Account<'a> {
    name: &'a str,
    parent: Option<Rc<RefCell<Account<'a>>>>,
    height: usize,
    emails: Vec<&'a str>,
}

impl Account<'_> {
    fn new(name: &str) -> Rc<RefCell<Account>> {
        Rc::new(RefCell::new(Account {
            name,
            parent: None,
            height: 0,
            emails: Vec::new(),
        }))
    }

    fn merge<'a>(lhs: Rc<RefCell<Account<'a>>>, rhs: Rc<RefCell<Account<'a>>>) {
        let lhs = Account::get_top(lhs);
        let rhs = Account::get_top(rhs);
        if Rc::ptr_eq(&lhs, &rhs) {
            return;
        }
        let lhs_height = lhs.borrow().height;
        let rhs_height = rhs.borrow().height;
        if lhs_height < rhs_height {
            lhs.borrow_mut().parent = Some(rhs);
        } else {
            lhs.borrow_mut().height = lhs_height.max(rhs_height + 1);
            rhs.borrow_mut().parent = Some(lhs);
        }
    }

    fn get_top(mut account: Rc<RefCell<Account>>) -> Rc<RefCell<Account>> {
        let mut stack = Vec::new();
        loop {
            if account.borrow().parent.is_some() {
                stack.push(Rc::clone(&account));
                let parent = account.borrow_mut().parent.take().unwrap();
                account = parent;
            } else {
                break;
            }
        }
        while let Some(a) = stack.pop() {
            a.borrow_mut().parent = Some(Rc::clone(&account));
        }
        account
    }
}

impl Solution {
    pub fn accounts_merge(accounts: Vec<Vec<String>>) -> Vec<Vec<String>> {
        let mut account_list = Vec::new();
        let mut email_to_account = BTreeMap::new();
        for account in &accounts {
            let name = &account[0];
            let cur_account = Account::new(name);
            account_list.push(Rc::clone(&cur_account));
            for email in account.iter().skip(1) {
                let email = &email[..];
                email_to_account
                    .entry(email)
                    .and_modify(|a| Account::merge(Rc::clone(a), Rc::clone(&cur_account)))
                    .or_insert(Rc::clone(&cur_account));
            }
        }
        for (email, account) in email_to_account {
            Account::get_top(account).borrow_mut().emails.push(email);
        }
        account_list
            .iter()
            .filter_map(|account| {
                if account.borrow().parent.is_some() {
                    None
                } else {
                    let mut a = Vec::new();
                    a.push(account.borrow().name.into());
                    a.extend(account.borrow().emails.iter().map(|&e| e.into()));
                    Some(a)
                }
            })
            .collect()
    }
}

pub fn main() {
    let accounts = vec![
        vec!["John", "johnsmith@mail.com", "john_newyork@mail.com"],
        vec!["John", "johnsmith@mail.com", "john00@mail.com"],
        vec!["Mary", "mary@mail.com"],
        vec!["John", "johnnybravo@mail.com"],
    ];
    println!(
        "{:?}",
        Solution::accounts_merge(
            accounts
                .into_iter()
                .map(|a| a.into_iter().map(|e| e.into()).collect())
                .collect()
        )
    );

    let accounts = vec![
        vec!["Gabe", "Gabe0@m.co", "Gabe3@m.co", "Gabe1@m.co"],
        vec!["Kevin", "Kevin3@m.co", "Kevin5@m.co", "Kevin0@m.co"],
        vec!["Ethan", "Ethan5@m.co", "Ethan4@m.co", "Ethan0@m.co"],
        vec!["Hanzo", "Hanzo3@m.co", "Hanzo1@m.co", "Hanzo0@m.co"],
        vec!["Fern", "Fern5@m.co", "Fern1@m.co", "Fern0@m.co"],
    ];
    println!(
        "{:?}",
        Solution::accounts_merge(
            accounts
                .into_iter()
                .map(|a| a.into_iter().map(|e| e.into()).collect())
                .collect()
        )
    );

    let accounts = vec![
        vec!["Alex", "Alex5@m.co", "Alex4@m.co", "Alex0@m.co"],
        vec!["Ethan", "Ethan3@m.co", "Ethan3@m.co", "Ethan0@m.co"],
        vec!["Kevin", "Kevin4@m.co", "Kevin2@m.co", "Kevin2@m.co"],
        vec!["Gabe", "Gabe0@m.co", "Gabe3@m.co", "Gabe2@m.co"],
        vec!["Gabe", "Gabe3@m.co", "Gabe4@m.co", "Gabe2@m.co"],
    ];
    println!(
        "{:?}",
        Solution::accounts_merge(
            accounts
                .into_iter()
                .map(|a| a.into_iter().map(|e| e.into()).collect())
                .collect()
        )
    );

    let accounts = vec![
        vec!["David", "David0@m.co", "David1@m.co"],
        vec!["David", "David3@m.co", "David4@m.co"],
        vec!["David", "David4@m.co", "David5@m.co"],
        vec!["David", "David2@m.co", "David3@m.co"],
        vec!["David", "David1@m.co", "David2@m.co"],
    ];
    println!(
        "{:?}",
        Solution::accounts_merge(
            accounts
                .into_iter()
                .map(|a| a.into_iter().map(|e| e.into()).collect())
                .collect()
        )
    );
}
