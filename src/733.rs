#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn flood_fill(mut image: Vec<Vec<i32>>, sr: i32, sc: i32, color: i32) -> Vec<Vec<i32>> {
        let (m, n) = (image.len(), image[0].len());
        let mut mark = vec![vec![false; n]; m];
        let mut stack = Vec::with_capacity(n * m);
        let old = image[sr as usize][sc as usize];
        mark[sr as usize][sc as usize] = true;
        stack.push((sr, sc));
        while let Some((x, y)) = stack.pop() {
            image[x as usize][y as usize] = color;
            let mut helper = |x: i32, y: i32| {
                if x >= 0
                    && (x as usize) < m
                    && y >= 0
                    && (y as usize) < n
                    && image[x as usize][y as usize] == old
                    && !mark[x as usize][y as usize]
                {
                    mark[x as usize][y as usize] = true;
                    stack.push((x, y));
                }
            };
            let d = [(-1, 0), (1, 0), (0, -1), (0, 1)];
            for (dx, dy) in d {
                helper(x + dx, y + dy);
            }
        }
        image
    }
}

pub fn main() {
    let image = vec![vec![1, 1, 1], vec![1, 1, 0], vec![1, 0, 1]];
    println!("{:?}", Solution::flood_fill(image, 1, 1, 2));

    let image = vec![vec![0, 0, 0], vec![0, 0, 0]];
    println!("{:?}", Solution::flood_fill(image, 0, 0, 0));
}
