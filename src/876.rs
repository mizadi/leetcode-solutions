#![allow(dead_code)]

pub struct Solution;
// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

impl Solution {
    pub fn middle_node(head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        let mut a = &head;
        let mut b = &head;
        let mut move_b = false;
        while let Some(a_int) = a {
            a = &a_int.next;
            if move_b {
                b = &b.as_ref().unwrap().next;
            }
            move_b = !move_b;
        }
        b.clone()
    }
}

pub fn main() {
    let head = Some(Box::new(ListNode {
        val: 1,
        next: Some(Box::new(ListNode {
            val: 2,
            next: Some(Box::new(ListNode {
                val: 3,
                next: Some(Box::new(ListNode {
                    val: 4,
                    next: Some(Box::new(ListNode {
                        val: 5,
                        next: Some(Box::new(ListNode { val: 6, next: None })),
                    })),
                })),
            })),
        })),
    }));
    println!(
        "{:?} -> {:?}",
        head.clone(),
        Solution::middle_node(head)
            .as_ref()
            .map(|e| e.val)
            .unwrap_or_default()
    );
}
