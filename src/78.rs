#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn subsets(nums: Vec<i32>) -> Vec<Vec<i32>> {
        fn generate_subsets(
            nums: &[i32],
            cur: &mut Vec<i32>,
            mut result: Vec<Vec<i32>>,
        ) -> Vec<Vec<i32>> {
            if nums.is_empty() {
                result.push(cur.to_vec());
            } else {
                result = generate_subsets(&nums[1..], cur, result);
                cur.push(nums[0]);
                result = generate_subsets(&nums[1..], cur, result);
                cur.pop();
            }
            result
        }
        generate_subsets(
            &nums,
            &mut Vec::with_capacity(nums.len()),
            Vec::with_capacity(2usize.pow(nums.len() as u32)),
        )
    }
}

pub fn main() {
    println!("{:?}", Solution::subsets(vec![1, 2, 3]));
    println!("{:?}", Solution::subsets(vec![0]));
}
