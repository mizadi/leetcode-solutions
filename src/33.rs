#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn search(nums: Vec<i32>, target: i32) -> i32 {
        use std::cmp::Ordering;
        let mut start = 0;
        let mut end = nums.len();
        while start < end {
            let mid = (start + end) / 2;
            match nums[mid].cmp(&target) {
                Ordering::Equal => return mid as i32,
                Ordering::Less => match nums[end - 1].cmp(&target) {
                    Ordering::Equal => return (end - 1) as i32,
                    Ordering::Less => {
                        if nums[mid] <= nums[end - 1] {
                            end = mid
                        } else {
                            start = mid + 1
                        }
                    }
                    Ordering::Greater => start = mid + 1,
                },
                Ordering::Greater => match nums[start].cmp(&target) {
                    Ordering::Equal => return start as i32,
                    Ordering::Less => end = mid,
                    Ordering::Greater => {
                        if nums[start] <= nums[mid] {
                            start = mid + 1
                        } else {
                            end = mid
                        }
                    }
                },
            }
        }
        -1
    }
}

pub fn main() {
    println!("{}", Solution::search(vec![4, 5, 6, 7, 0, 1, 2], 0));
    println!("{}", Solution::search(vec![4, 5, 6, 7, 0, 1, 2], 3));
    println!("{}", Solution::search(vec![1], 0));
    println!("{}", Solution::search(vec![7, 8, 1, 2, 3, 4, 5, 6], 2));
}
