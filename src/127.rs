#![allow(dead_code)]

struct Solution;
impl Solution {
    pub fn ladder_length(begin_word: String, end_word: String, mut word_list: Vec<String>) -> i32 {
        let end_word = match word_list.iter().position(|w| w == &end_word) {
            Some(position) => position,
            None => return 0,
        };
        if begin_word == word_list[end_word] {
            return 0;
        }
        word_list.push(begin_word);
        let connected = |w1: usize, w2: usize| -> bool {
            word_list[w1]
                .as_bytes()
                .iter()
                .zip(word_list[w2].as_bytes().iter())
                .try_fold(false, |connected, (&a, &b)| match (connected, a == b) {
                    (_, true) => Some(connected),
                    (false, false) => Some(true),
                    (true, false) => None,
                })
                .unwrap_or(false)
        };
        let n = word_list.len();
        let mut mark = vec![false; n];
        let mut q = Vec::new();
        let mut dist = 1;
        q.push(n - 1);
        mark[n - 1] = true;
        while !q.is_empty() {
            dist += 1;
            let mut next_q = Vec::new();
            for cur in q {
                for i in 0..n {
                    if !mark[i] && connected(i, cur) {
                        if i == end_word {
                            return dist;
                        }
                        next_q.push(i);
                        mark[i] = true;
                    }
                }
            }
            q = next_q;
        }
        0
    }
}

pub fn main() {
    println!(
        "{}",
        Solution::ladder_length(
            "hit".into(),
            "cog".into(),
            vec![
                "hot".into(),
                "dot".into(),
                "dog".into(),
                "lot".into(),
                "log".into(),
                "cog".into()
            ]
        )
    );
    println!(
        "{}",
        Solution::ladder_length(
            "hit".into(),
            "cog".into(),
            vec![
                "hot".into(),
                "dot".into(),
                "dog".into(),
                "lot".into(),
                "log".into(),
            ]
        )
    );
}
