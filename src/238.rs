#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn product_except_self(nums: Vec<i32>) -> Vec<i32> {
        let n = nums.len();
        let mut result = vec![1; n];
        for i in 1..n {
            result[i] = result[i - 1] * nums[i - 1];
        }
        let mut mul = 1;
        for i in (0..n - 1).rev() {
            mul *= nums[i + 1];
            result[i] *= mul;
        }
        result
    }
}

pub fn main() {
    println!("{:?}", Solution::product_except_self(vec![1, 2, 3, 4]));
    println!("{:?}", Solution::product_except_self(vec![-1, 1, 0, -3, 3]));
    println!(
        "{:?}",
        Solution::product_except_self(vec![1, 0, 2, 3, 0, 4])
    );
}
