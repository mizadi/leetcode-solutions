#![allow(dead_code)]

struct Solution;

#[derive(PartialEq, Debug)]
enum Stage {
    Leading,
    Sign,
    Digits,
}

const MAX: i64 = i32::MAX as i64;
const MIN: i64 = i32::MIN as i64;

impl Solution {
    pub fn my_atoi(s: String) -> i32 {
        let mut num = 0_i64;
        let mut neg = false;
        let mut stage = Stage::Leading;
        for ch in s.chars() {
            if stage == Stage::Leading {
                if ch == ' ' {
                    continue;
                }
                stage = Stage::Sign;
            }
            if stage == Stage::Sign {
                stage = Stage::Digits;
                if ch == '-' {
                    neg = true;
                    continue;
                }
                if ch == '+' {
                    continue;
                }
            }
            if stage == Stage::Digits {
                if ch.is_ascii_digit() {
                    if num < MAX {
                        num = num * 10 + ch as i64 - '0' as i64;
                    }
                    continue;
                }
                break;
            }
        }
        if neg {
            MIN.max(-num) as i32
        } else {
            MAX.min(num) as i32
        }
    }
}

pub fn main() {
    println!("{}", Solution::my_atoi("42".into()));
    println!("{}", Solution::my_atoi("    -42".into()));
    println!("{}", Solution::my_atoi("4193 with words".into()));
    println!("{}", Solution::my_atoi("-91283472332".into()));
}
