#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn search(nums: Vec<i32>, target: i32) -> i32 {
        let (mut start, mut end) = (0, nums.len());
        while start < end {
            let mid = (start + end) / 2;
            if target < nums[mid] {
                end = mid;
            } else if target > nums[mid] {
                start = mid + 1;
            } else {
                return mid as i32;
            }
        }
        -1
    }
}

pub fn main() {
    let nums = vec![-1, 0, 3, 5, 9, 12];
    let target = 9;
    println!(
        "{} in {:?} at {}",
        target,
        nums.clone(),
        Solution::search(nums, target)
    );

    let nums = vec![-1, 0, 3, 5, 9, 12];
    let target = 2;
    println!(
        "{} in {:?} at {}",
        target,
        nums.clone(),
        Solution::search(nums, target)
    );
}
