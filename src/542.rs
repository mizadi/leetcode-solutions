#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn update_matrix(mut mat: Vec<Vec<i32>>) -> Vec<Vec<i32>> {
        let m = mat.len();
        let n = mat[0].len();
        let mut q = std::collections::VecDeque::new();
        for i in 0..m {
            for j in 0..n {
                if mat[i][j] == 0 {
                    q.push_back((i, j));
                } else {
                    mat[i][j] = -1;
                }
            }
        }
        while let Some((i, j)) = q.pop_front() {
            let nextd = mat[i][j] + 1;
            if i > 0 && mat[i - 1][j] == -1 {
                mat[i - 1][j] = nextd;
                q.push_back((i - 1, j));
            }
            if i < m - 1 && mat[i + 1][j] == -1 {
                mat[i + 1][j] = nextd;
                q.push_back((i + 1, j));
            }
            if j > 0 && mat[i][j - 1] == -1 {
                mat[i][j - 1] = nextd;
                q.push_back((i, j - 1));
            }
            if j < n - 1 && mat[i][j + 1] == -1 {
                mat[i][j + 1] = nextd;
                q.push_back((i, j + 1));
            }
        }
        mat
    }
}

pub fn main() {
    println!("{:?}", Solution::update_matrix(vec![vec![0,0,0],vec![0,1,0],vec![0,0,0]]));
    println!("{:?}", Solution::update_matrix(vec![vec![0,0,0],vec![0,1,0],vec![1,1,1]]));
}
