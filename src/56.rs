#![allow(dead_code)]

pub struct Solution;
impl Solution {
    pub fn merge(mut intervals: Vec<Vec<i32>>) -> Vec<Vec<i32>> {
        intervals.sort_unstable();
        let mut result = Vec::with_capacity(intervals.len());
        for interval in intervals {
            match result.last_mut() {
                None => result.push(interval),
                Some(last) => {
                    if last[1] < interval[0] {
                        result.push(interval);
                    } else {
                        last[1] = last[1].max(interval[1]);
                    }
                }
            }
        }
        result
    }
}

pub fn main() {
    println!(
        "{:?}",
        Solution::merge(vec![vec![1, 3], vec![2, 6], vec![8, 10], vec![15, 18]])
    );
    println!("{:?}", Solution::merge(vec![vec![1, 4], vec![4, 5]]));
}
